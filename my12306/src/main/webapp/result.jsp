<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>查询结果-${from }-${to }</title>
    <meta charset="utf-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" href="static/css/style.css" />
	<script type="text/javascript">
		document.addEventListener('plusready', function() {
		//console.log("所有plus api都应该在此事件发生后调用，否则会出现plus is undefined。"
		});
	</script>
  </head>
  
  <body>
    <header>Easy-To-Query
			<span>My12306</span>
	</header>
	<div class="con">
		<h3>以下是查询结果</h3>
		<c:forEach items = "${trains}" var = "train">
		<div class="ticket">
			<table>
				<tr>
					<td class="star">${train.from_station_name}</td>
					<td>${train.station_train_code}</td>
					<td class="end">${train.to_station_name}</td>
					<td>(¬_¬)</td>
				</tr>
				<tr>
					<td class="startime">${train.start_time}</td>
					<td class="during">${train.lishi}</td>
					<td class="endtime">${train.arrive_time}</td>
					<td class="hasticket">有票</td>
				</tr>
				<c:choose>  
                	<c:when test="${train.isGD == '1'}">
                	<tr>
						<td>特等座：${train.tz_num}</td>
						<td>商务座：${train.swz_num}</td>
						<td>一等座：${train.zy_num}</td>
						<td>二等座：${train.ze_num}</td>
					</tr>
                	</c:when>  
                	<c:otherwise>
                	<tr>
						<td>软卧：${train.rw_num}</td>
						<td>硬卧：${train.yw_num}</td>
						<td>硬座：${train.yz_num}</td>
						<td>无座：${train.wz_num}</td>
					</tr>
                	</c:otherwise>  
            	</c:choose> 
				
			</table>
		</div>
		</c:forEach>
	</div>
	<a href="/my12306/m" class="back">
			
	</a>
  </body>
</html>
