<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>车票查询${from }-${to }</title>
    <meta chartset="utf-8">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  	
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">My12306</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            <form action="/my12306/ticket/query" method="post" class="navbar-form navbar-left" role="search">
                <div class="form-group">
                    <input id="from_station" name="from" value="${from }" type="text" class="form-control" placeholder="出发地" required>
                </div>
                <div class="form-group">
                    <input type="text" name="to" value="${to}" class="form-control" placeholder="目的地" required>
                </div>
                <div class="form-group">
                    <input type="date" name="date" value="${date }" class="form-control" placeholder="出行日期" required>
                </div>
                <button type="submit" class="btn btn-default">查询</button>
            </form>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container" style="margin-top:80px;">
    <table class="table table-hover table-condensed text-center">
        <thead>
        <tr>
            <th class="text-center">车次</th>
            <th class="text-center">路线</th>
            <th class="text-center">时间</th>
            <th class="text-center" width="120px">一等座</th>
            <th class="text-center" width="120px">二等座</th>
            <th class="text-center" width="120px">软卧</th>
            <th class="text-center" width="120px">硬卧</th>
            <th class="text-center" width="120px">硬座</th>
            <th class="text-center" width="120px">无座</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items = "${trains}" var = "train">
        <tr>
            <td><span style="font-size:19px; color:#E43A45;">${train.station_train_code}</span></td>
            <td>
            	<label class="label label-primary">${train.from_station_name}</label>
            	&rarr;
            	<label class="label label-success">${train.to_station_name}</label>
            </td>
            <td>
            	<label class="label label-warning">${train.start_time}</label>
            	&rarr;
            	<label class="label label-danger">${train.arrive_time}</label>
            </td>
            <td>${train.zy_num}</td>
            <td>${train.ze_num}</td>
            <td>${train.rw_num}</td>
            <td>${train.yw_num}</td>
            <td>${train.yz_num}</td>
            <td>${train.wz_num}</td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="http://cdn.bootcss.com/jquery/1.11.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
  </body>
</html>
