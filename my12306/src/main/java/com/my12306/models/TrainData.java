package com.my12306.models;

import java.util.ArrayList;
import java.util.List;

public class TrainData {
	private List<TrainEnquiries> datas = new ArrayList<TrainEnquiries>();
	private String searchDate;
	private boolean flag;
	public List<TrainEnquiries> getDatas() {
		return datas;
	}
	public void setDatas(List<TrainEnquiries> datas) {
		this.datas = datas;
	}
	public String getSearchDate() {
		return searchDate;
	}
	public void setSearchDate(String searchDate) {
		this.searchDate = searchDate;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	
}
