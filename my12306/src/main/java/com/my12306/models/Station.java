package com.my12306.models;

public class Station {
	private String station_name_jian;
	private String station_name;
	private String station_code;
	private String station_name_pinyin;
	private String station_name_jianpin;
	private String station_id;
	public String getStation_name_jian() {
		return station_name_jian;
	}
	public void setStation_name_jian(String station_name_jian) {
		this.station_name_jian = station_name_jian;
	}
	public String getStation_name() {
		return station_name;
	}
	public void setStation_name(String station_name) {
		this.station_name = station_name;
	}
	public String getStation_code() {
		return station_code;
	}
	public void setStation_code(String station_code) {
		this.station_code = station_code;
	}
	public String getStation_name_pinyin() {
		return station_name_pinyin;
	}
	public void setStation_name_pinyin(String station_name_pinyin) {
		this.station_name_pinyin = station_name_pinyin;
	}
	public String getStation_name_jianpin() {
		return station_name_jianpin;
	}
	public void setStation_name_jianpin(String station_name_jianpin) {
		this.station_name_jianpin = station_name_jianpin;
	}
	public String getStation_id() {
		return station_id;
	}
	public void setStation_id(String station_id) {
		this.station_id = station_id;
	}
	
	
}
