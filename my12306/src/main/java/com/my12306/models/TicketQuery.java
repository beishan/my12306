package com.my12306.models;

public class TicketQuery {
	private String validateMessagesShowId;
	private boolean status;
	private TrainData data;
	private Object messages [];
	private Object validateMessages;
	
	public void initIsGD(){
		for(TrainEnquiries te : data.getDatas()){
			te.initIsGD();
		}
	}
	
	
	public String getValidateMessagesShowId() {
		return validateMessagesShowId;
	}
	public void setValidateMessagesShowId(String validateMessagesShowId) {
		this.validateMessagesShowId = validateMessagesShowId;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public Object[] getMessages() {
		return messages;
	}
	public void setMessages(Object[] messages) {
		this.messages = messages;
	}
	public Object getValidateMessages() {
		return validateMessages;
	}
	public void setValidateMessages(Object validateMessages) {
		this.validateMessages = validateMessages;
	}
	public TrainData getData() {
		return data;
	}
	public void setData(TrainData data) {
		this.data = data;
	}
	
}
