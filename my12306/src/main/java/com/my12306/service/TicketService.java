package com.my12306.service;

public interface TicketService {
	public String test();
	/**
	 * query ticket 4 param
	 * @param purpose_codes
	 * @param queryDate
	 * @param from_station
	 * @param to_station
	 * @return
	 */
	public String httpQueryTicket(String purpose_codes, String queryDate, String from_station, String to_station) throws Exception;
	
	/**
	 * query ticket 3 param
	 * @param queryDate
	 * @param from_station
	 * @param to_station
	 * @return
	 */
	public String httpQueryTicket(String queryDate, String from_station, String to_station) throws Exception;
}
