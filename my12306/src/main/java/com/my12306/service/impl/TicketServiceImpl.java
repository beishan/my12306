package com.my12306.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.my12306.service.TicketService;
import com.my12306.util.HttpClientUtils;
import com.my12306.util.HttpUtil;

@Service(value = "ticketService")
public class TicketServiceImpl extends BaseService implements TicketService{

	@Override
	public String httpQueryTicket(String purpose_codes, String queryDate,
			String from_station, String to_station) throws Exception {
		String url = "https://kyfw.12306.cn/otn/lcxxcx/query";
		String param = "purpose_codes=" + purpose_codes + "&queryDate=" + queryDate + "&from_station=" + from_station + "&to_station=" + to_station;
		
		url = url + "?" + param;
		return HttpClientUtils.get(url);
	}

	@Override
	public String httpQueryTicket(String queryDate, String from_station,
			String to_station) throws Exception {
		String purpose_codes = "ADULT";
		
		return this.httpQueryTicket(purpose_codes, queryDate, from_station, to_station);
	}

	@Override
	public String test() {
		// TODO Auto-generated method stub
		System.out.println("hello");
		return null;
	}

}
