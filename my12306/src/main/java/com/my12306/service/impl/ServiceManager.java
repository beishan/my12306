package com.my12306.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.my12306.service.TicketService;

@Service
public class ServiceManager {
	@Autowired
	private TicketService ticKetService;

	public TicketService getTicKetService() {
		return ticKetService;
	}

	public void setTicKetService(TicketService ticKetService) {
		this.ticKetService = ticKetService;
	}
	
}
