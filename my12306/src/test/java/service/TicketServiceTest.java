package service;

import org.junit.Test;

import util.BaseTest;

import com.google.gson.Gson;
import com.my12306.models.TicketQuery;
import com.my12306.models.TrainEnquiries;

public class TicketServiceTest extends BaseTest {

	@Test
	public void test1(){
		serviceManager.getTicKetService().test();
	}
	
	@Test
	public void testQueryTicket() throws Exception{
		String queryDate = "2016-10-06";
		String from_station = "SHH";
		String to_station = "BJP";
		String result = serviceManager.getTicKetService().httpQueryTicket(queryDate, from_station, to_station);
		System.out.println(result);
		
		Gson gson = new Gson();
		
		TicketQuery tq = gson.fromJson(result, TicketQuery.class);
		System.out.println("出发站\t 到达站\t 出发时间\t 到达时间");
		for(TrainEnquiries te : tq.getData().getDatas()){
			System.out.println(te);
		}
	}
}
